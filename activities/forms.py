from django.forms.models import ModelForm
from .models import Peserta,Kegiatan
from django import forms

class KegiatanForm(forms.Form):
    nama_kegiatan = forms.CharField(label="Nama Kegiatan", max_length=64,
    widget=forms.TextInput(attrs={'class': 'form-control'}))

class PesertaForm(forms.Form):
    nama_peserta = forms.CharField(label="Ikuti acara", max_length=64,
    widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Masukan Nama'}))