from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length = 30)

    def __str__(self):
        return self.nama

class Peserta(models.Model):
    nama = models.CharField(max_length = 30)
    kegiatan = models.ForeignKey(Kegiatan,on_delete = models.DO_NOTHING)

    def __str__(self):
        return self.nama
 