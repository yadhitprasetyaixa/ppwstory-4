from django.shortcuts import render,redirect
from .forms import KegiatanForm,PesertaForm
from .models import Kegiatan,Peserta

# Create your views here.
def activities(request):
    data ={
        'kegiatan':Kegiatan.objects.all(),
        'peserta':Peserta.objects.all(),
    }
    form_data ={
        'form_kegiatan':KegiatanForm,
        'form_peserta': PesertaForm,
    }
    content = {
        'nbar':'activities',
        'forms':form_data,
        'database':data
        }
    return render(request, 'activities/act.html', content)

def post_kegiatan(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST or None)
        if form.is_valid():
            data_form = form.cleaned_data
            data = Kegiatan(nama=data_form['nama_kegiatan'])
            data.save()
            return redirect('/activities/')
        else:
            return redirect('/activities/')
    else:
        return redirect('/activities/')
    
def post_peserta(request, id_kegiatan):
    if request.method == 'POST':
        form = PesertaForm(request.POST or None)
        if form.is_valid():
            kegiatan = Kegiatan.objects.get(id=id_kegiatan)
            data_form = form.cleaned_data
            data_input = Peserta()
            data_input.nama = data_form['nama_peserta']
            data_input.kegiatan = kegiatan

            data_input.save()
            return redirect('/activities/')
        else:
            return redirect('/activities/')
    else:
        return redirect('/activities/')