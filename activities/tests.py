from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *


# Create your tests here.
class activitiestest(TestCase):

    def test_url_exist(self):
            response = Client().get('')
            self.assertEqual(response.status_code,200)

    def test_create_models_Kegiatan(self):
            new_model = Kegiatan.objects.create(nama ='PUSING')
            counting_new_model = Kegiatan.objects.all().count()
            self.assertEqual(counting_new_model,1)

    def test_create_models_Peserta(self):
        new_model = Kegiatan.objects.create(nama ='PUSING')
        new_peserta = Peserta.objects.create(nama ='yay', kegiatan = new_model)
        counting_new_model = Peserta.objects.all().count()
        self.assertEqual(counting_new_model,1)


    def test_views_activities(self):
            found=resolve('/activities/')
            self.assertEqual(found.func,activities)

    def test_views_post_kegiatan(self):
            found=resolve('/activities/post-kegiatan')
            self.assertEqual(found.func,post_kegiatan)

    def test_activities_template_used(self):
            response = Client().get('/activities/')
            self.assertTemplateUsed(response, 'activities/act.html')
