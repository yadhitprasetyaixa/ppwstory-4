from django.urls import path

from .import views

app_name = 'activities'

urlpatterns = [
    path('', views.activities, name='activities'),
    path('post-kegiatan',views.post_kegiatan),
    path('post-peserta/<int:id_kegiatan>',views.post_peserta),
]