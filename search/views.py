from django.shortcuts import render
from django.http import JsonResponse,HttpRequest
import requests
import json
# Create your views here.
def search(request): 
    context = {
        'nbar':'search',
    }
    return render(request, 'search/search.html',context)

def caridata(request):
    arg = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    isi = requests.get(url_tujuan)
    data = json.loads(isi.content)
    return JsonResponse(data, safe = False)