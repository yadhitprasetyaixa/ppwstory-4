from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *



# Create your tests here.
class activitiestest(TestCase):

    def test_url_exist(self):
            response = Client().get('')
            self.assertEqual(response.status_code,200)

    def test_views_activities(self):
            found=resolve('/search/')
            self.assertEqual(found.func,search)

    def test_activities_template_used(self):
            response = Client().get('/search/')
            self.assertTemplateUsed(response, 'search/search.html')
