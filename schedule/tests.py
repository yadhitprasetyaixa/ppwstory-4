from django.test import TestCase, Client

# Create your tests here.
class TestSchedule(TestCase):

    def test_url(self):
        response = Client().get('/schedule/')
        self.assertEquals(200, response.status_code)

    def test_form(self):
        response = Client().get('/schedule/add/')
        isii = response.content.decode('utf8')
        self.assertIn('<form action="." method="POST">',isii)

    def test_isi(self):
        response = Client().get('/schedule/')
        isii = response.content.decode('utf8')

        self.assertIn("Mata Kuliah",isii)
        self.assertIn("Semester",isii)
        self.assertIn("Jumlah SKS",isii)
        self.assertIn("Nama Dosen",isii)
        self.assertIn("Ruang Kuliah",isii)
        self.assertIn("Deskripsi",isii)
        self.assertIn("Mata Kuliah",isii)
