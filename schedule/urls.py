from django.urls import path

from . import views

app_name = 'schedule'

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('add/', views.add, name='addschedule'),
    path('delete/<str:pk>/',views.delete, name='delete'),
]