from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .models import Matkul

# Create your views here.
def schedule(request): 
    matkuls = Matkul.objects.all()
    context = {
        'object_list': matkuls,
        'nbar':'schedule',
    }
    return render(request, 'schedule/schedule.html',context)

def add(request):
    parameter = False
    nama = ""
    if request.method == "POST":
        parameter = True
        mata = request.POST.get("nama")
        semester = request.POST.get("semester")
        sks = request.POST.get("sks")
        dosen = request.POST.get("dosen")
        ruang = request.POST.get("ruang")
        desk = request.POST.get("desc")
        p = Matkul.objects.create(nama_matkul = mata, semester_tahun = semester, jumlah_sks = sks, nama_dosen = dosen, ruang_kuliah = ruang, deskripsi = desk)
        p.save()
        nama = mata
        return redirect("/schedule")

    context = {
        'par' : parameter,
        'nbar':'schedule',
        'tes': nama,

    }
    return render(request, 'schedule/add.html',context)

def delete(request, pk):
    item=Matkul.objects.get(id=pk)
    if request.method == "POST":
        item.delete()
        return redirect("/schedule")
    context ={
        'nbar':'schedule',
        'matkul': item,
    }
    return render(request,'schedule/delete.html',context)

def savecourse(request):
    return HttpResponseRedirect('/schedule')