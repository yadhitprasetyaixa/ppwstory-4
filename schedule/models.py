from django.db import models

# Create your models here.
class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=50)
    semester_tahun = models.CharField(max_length=20)
    jumlah_sks = models.CharField(max_length=10, default=0)
    nama_dosen = models.CharField(max_length=50)
    ruang_kuliah = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=100)