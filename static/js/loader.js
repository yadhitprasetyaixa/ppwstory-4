$(document).ready(function() {
    $('body').addClass('loaded');

    $('#activitySatu').click(function() {
        $('#content-satu').slideToggle("slow", function() {
            //uhuy
        });
    });

    $('#activityDua').click(function() {
        $('#content-dua').slideToggle("slow", function() {
            //uhuy
        });
    });

    $('#activityTiga').click(function() {
        $('#content-tiga').slideToggle("slow", function() {
            //uhuy
        });
    });

    $('#activityEmpat').click(function() {
        $('#content-empat').slideToggle("slow", function() {
            //uhuy
        });
    });


    $(".up-button").click(function() {
        var content = $(this).parent().parent().parent().parent();
        content.insertBefore(content.prev());
    });

    $(".down-button").click(function() {
        var content = $(this).parent().parent().parent().parent();
        content.insertAfter(content.next());
    });

    $('#button-cari').click(function() {
        var keyword = $('#cari').val();
        var url_ini = '/data?q=' + keyword;
        $.ajax({
            url: url_ini,
            success: function(hasil) {

                var obj_hasil = $('.pencarian');
                obj_hasil.empty()

                for (i = 0; i < hasil.items.length; i++) {
                    var title = hasil.items[i].volumeInfo.title;
                    var gambar = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                    var penulis = hasil.items[i].volumeInfo.authors;
                    var terbit = hasil.items[i].volumeInfo.publishedDate;
                    var kategori = hasil.items[i].volumeInfo.categories;
                    var tebal = hasil.items[i].volumeInfo.pageCount;
                    obj_hasil.append('<div class="item"><img src= ' + gambar + '><div class = "detailbuku"><p>Judul Buku : ' + title + '</p><p>Penulis : ' + penulis + '</p><p>Tahun Terbit : ' + terbit + '</p><p>Kategori : ' + kategori + '</p><p>Jumlah Halaman : ' + tebal + ' halaman</p></div></div>');
                }
            }
        })
    });
});